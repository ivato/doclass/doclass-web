# Doclass

A tool for the interactive annotation and classification of juridic documents

## Doclass projects

You can see doclass-web (this repo), [doclass-mobile](https://gitlab.com/ivato/doclass/doclass-backend) and [doclass-backend](https://gitlab.com/ivato/doclass/doclass-mobile)

### Prerequisites

- NodeJS
- Yarn (Recommended)

### Starting this project

```bash
Clone this project
$ git clone https://gitlab.com/ivato/doclass/doclass-web

Access the project folder
$ cd doclass-web

Install the dependencies
$ yarn
or
$ npm install

lauch the frontend web application
$ yarn start
or
$ npm start

```

### How to contribute?

DoClass is an open source software which accept any contributing of the community who has the desire to help with the project.To contributing we can offer two options and the contributor can choose which is better for him, the first one consist in create an issue to inform bugs or suggest some upgrades in specific parts of the project. The second option is sending for us the changes that could be incorporate in the project to upgrade or evolve it and for this follow these instructions:  
-  Do a fork of the project;
-  After that create a branch with the modifications;
-  Push to the repository;
-  Request a pull for we can do the merge with the modifications in the master branch;

As you can see, it is simple how to help us with this project, and we thank you in advance for every help from the community.
