import React, {ButtonHTMLAttributes} from 'react'
import Loader from 'react-loader-spinner';

import '../styles/components/buttonLarge.css';

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  text: string;
  background: string;
  color: string;
  loading: boolean;
  icon: any;
}

const ButtonLarge: React.FC<Props> = ({
  text,
  loading,
  background,
  color,
  type,
  icon,
  ...rest
}) => {
  return (
    <button {...rest} style={{background:background, color: color}} id="button-large-container">
      {text}
      {!loading ? icon : <div id="loading-container">
        <Loader
         type="Oval"
         color={color}
         height={25}
        />
      </div>}
    </button>
  );
}

export default ButtonLarge;