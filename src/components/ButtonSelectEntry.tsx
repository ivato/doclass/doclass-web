import React, {ButtonHTMLAttributes} from 'react'

import '../styles/components/buttonSelectEntry.css';

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  text: string;
  background: string;
  color: string;
  icon: any;
}

const ButtonSelectEntry: React.FC<Props> = ({
  text,
  background,
  color,
  icon,
  ...rest
}) => {
  return (
    <button {...rest} style={{background:background, color: color}} id="button-select-entry-container">
      {text}
      {icon}
    </button>
  );
}

export default ButtonSelectEntry;