import React, {InputHTMLAttributes} from 'react'
import {Field} from 'formik'

import '../styles/components/input.css'

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  error: string | undefined;
  touched: boolean | undefined;
  textError: string | undefined;
  pressIcon: boolean | undefined;
  validate: (value: string) => string | undefined;
  iconFunction: () => void | null;
  icon: any;
}

const Input: React.FC<Props> = ({
  error,
  touched,
  textError,
  icon,
  pressIcon,
  iconFunction,
  ...rest
}) => {
  return (
    <div id="container">
      <div id="input-container">
        <button className={pressIcon ? "mouseHoverIcon" : ""} type="button" onClick={iconFunction} id="icon-container">
          {icon}
        </button>
        <Field id="input" {...rest}/>
      </div>
      <p id="text_error">{error && touched && textError}</p>
    </div>
  );
}

export default Input;