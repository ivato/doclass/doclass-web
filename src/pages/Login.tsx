import React, { useState } from 'react'
import { Formik, Form } from "formik";
import {FiMail, FiLock, FiUnlock, FiArrowRight} from 'react-icons/fi'

import '../styles/pages/login.css'
import Input from '../components/Input';
import ButtonLarge from '../components/ButtonLarge';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../store/hooks';
import { setEmail } from '../store/slices/auth';

export default function Login(){
  const [showPassword, setShowPassword] = useState(false);
  const email = useAppSelector(store => store.auth.email);
  const dispatch = useAppDispatch();

  function validateEmail(value: string) {
    let error;
    
    if (!value) {
      error = "Entry an email address";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)) {
      error = "Invalid email address";
    }
    
    return error;
  }

  function validatePassword(value: string) {
    let error;
    
    if (value === "") {
      error = "Entry a password!";
    }
    
    return error;
  }

  return (
    <div id="page-login">
      <div id="login-container">
        <p id="container-title">Welcome to Doclass</p>
        <Formik
          initialValues={{
            email,
            //password: ""
          }}
          onSubmit={(values, actions) => {
            dispatch(setEmail(values.email));
            actions.setSubmitting(false);
          }}
        >
          {({ values, handleChange, handleSubmit, errors, touched }) => (
            <Form>
              <Input 
                name="email"
                placeholder="Email"
                error={errors.email}
                touched={touched.email}
                textError={errors.email}
                pressIcon={false}
                icon={<FiMail size={26} color="#FFFFFF"/>}
                iconFunction={() => null}
                validate={validateEmail}/>

              {/* <Input 
                name="password"
                type={showPassword ? "text" : "password"}
                error={errors.password}
                touched={touched.password}
                textError={errors.password}
                pressIcon={true}
                icon={showPassword ? 
                      <FiUnlock size={26} color="#FFFFFF"/> : 
                      <FiLock size={26} color="#FFFFFF"/> }
                iconFunction={() => setShowPassword(!showPassword)}
                validate={validatePassword}/> */}

              <div className="loginButtonContainer">
                <ButtonLarge
                  onClick={() => handleSubmit()}
                  loading={false}
                  background="#68a1fb"
                  color="#ffffff"
                  text="Login"
                  icon={<FiArrowRight size={30}
                  color="#FFFFFF"/>} />
              </div>

              {/* <Link to="/register" className="register-button">
                Create an account
              </Link> */}
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}