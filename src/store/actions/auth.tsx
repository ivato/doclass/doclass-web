export const SETEMAIL = '@auth/SET_EMAIL'

export function setEmail(email: String) {
  return {
    type: SETEMAIL,
    payload: email,
  }
}