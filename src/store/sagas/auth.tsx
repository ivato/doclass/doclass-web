import { takeLatest, call, put, all, select } from 'redux-saga/effects';
import { SETEMAIL } from '../actions/auth';

export function* signIn() {
  yield console.log('sagas OK')
}


export default all([
  takeLatest("auth/setEmail", signIn),
]);
