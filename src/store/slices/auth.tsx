import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import moment from 'moment'

interface AuthState {
  email: string,
  session: string
}

const initialState = { email: "fernandoseverino@discente.ufg.br" } as AuthState


const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setEmail(state, action: PayloadAction<string>) {
      state.email = action.payload
    },

    newSession(state, action: PayloadAction<string>) {
      state.session = `${state.email}${moment.utc().unix()}`
    },
  },
})

export const { setEmail, newSession } = auth.actions
export default auth.reducer